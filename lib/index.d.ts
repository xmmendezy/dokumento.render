/// <reference types="node" />
declare module "interface/format.interface" {
    interface DokumentoFormat {
        pdfVersion?: number;
    }
    export { DokumentoFormat };
}
declare module "core/base.core" {
    class DokumentoBase {
        constructor();
        buffers: Buffer[];
        write(element: string | Buffer): void;
        get buffer(): Buffer;
    }
    export { DokumentoBase };
}
declare module "util/versions.utils" {
    const PDFVersions: {
        [key: string]: number;
    };
    export { PDFVersions };
}
declare module "index" {
    import { DokumentoFormat } from "interface/format.interface";
    import { DokumentoBase } from "core/base.core";
    export class DokumentoRender extends DokumentoBase {
        constructor(data: DokumentoFormat);
        pdfVersion: number;
        render_header(): void;
    }
}
declare module "interface/pdf.interface" {
    interface IPdfRoot {
    }
    export { IPdfRoot };
}
declare module "core/pdf.core" {
    import { IPdfRoot } from "interface/pdf.interface";
    class PdfRoot {
        constructor(data: IPdfRoot);
    }
    export { PdfRoot };
}
declare module "util/margin.util" {
    interface IDokumentoMargin {
        top: number;
        bottom?: number;
        left: number;
        right?: number;
    }
    class DokumentoMargin implements IDokumentoMargin {
        constructor(data: IDokumentoMargin | number);
        top: number;
        bottom: number;
        left: number;
        right: number;
    }
    export { IDokumentoMargin, DokumentoMargin };
}
