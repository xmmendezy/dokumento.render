define("interface/format.interface", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
});
define("core/base.core", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class DokumentoBase {
        constructor() {
            this.buffers = [];
        }
        write(element) {
            if (!Buffer.isBuffer(element)) {
                element = new Buffer(`${element}\n`, 'binary');
            }
            this.buffers = this.buffers.concat(element);
        }
        get buffer() {
            return Buffer.concat(this.buffers);
        }
    }
    exports.DokumentoBase = DokumentoBase;
});
define("util/versions.utils", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const PDFVersions = {
        '1.3': 1.3,
        '1.4': 1.4,
        '1.5': 1.5,
        '1.6': 1.6,
        '1.7': 1.7,
        '1.7ext3': 1.7,
    };
    exports.PDFVersions = PDFVersions;
});
define("index", ["require", "exports", "core/base.core", "util/versions.utils"], function (require, exports, base_core_1, versions_utils_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class DokumentoRender extends base_core_1.DokumentoBase {
        constructor(data) {
            super();
            this.pdfVersion = data.pdfVersion ? (versions_utils_1.PDFVersions[data.pdfVersion] ? versions_utils_1.PDFVersions[data.pdfVersion] : 1.3) : 1.3;
            this.render_header();
        }
        render_header() {
            this.write(`%PDF-${this.pdfVersion}`);
            this.write('%\xFF\xFF\xFF\xFF');
        }
    }
    exports.DokumentoRender = DokumentoRender;
});
define("interface/pdf.interface", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
});
define("core/pdf.core", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class PdfRoot {
        constructor(data) { }
    }
    exports.PdfRoot = PdfRoot;
});
define("util/margin.util", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class DokumentoMargin {
        constructor(data) {
            if (typeof data === 'number') {
                data = {
                    top: data,
                    left: data,
                };
            }
            this.top = data.top;
            this.bottom = data.bottom ? data.bottom : data.top;
            this.left = data.left;
            this.right = data.right ? data.right : data.left;
        }
    }
    exports.DokumentoMargin = DokumentoMargin;
});
//# sourceMappingURL=index.js.map