class DokumentoBase {
	constructor() {
		this.buffers = [];
	}

	public buffers: Buffer[];

	public write(element: string | Buffer): Buffer {
		if (!Buffer.isBuffer(element)) {
			element = new Buffer(`${element}\n`, 'binary');
		}
		this.buffers = this.buffers.concat(element);
		return element;
	}

	get buffer(): Buffer {
		return Buffer.concat(this.buffers);
	}
}

export { DokumentoBase };
