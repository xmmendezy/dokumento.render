//Const
import { DEFAULT_MARGINS } from '@/util/margin.util';
//Interface
import { IDokumentoPage } from '@/interface/page.interface';
//Class
import { DokumentoRender } from '@/index';
import { DokumentoMargin } from '@/util/margin.util';

class DokumentoPage implements IDokumentoPage {
	constructor(data?: IDokumentoPage) {
		if (!data) {
			data = {};
		}
		this.size = data.size ? data.size : 'letter';
		this.size = this.size.toUpperCase();
		this.layout = data.layout ? data.layout : 'PORTRAIT';
		this.margin = data.margin ? new DokumentoMargin(data.margin) : DEFAULT_MARGINS;
	}

	public document: DokumentoRender | undefined;
	public size: string;
	public layout: 'PORTRAIT' | 'LANDSPACE';
	public margin: DokumentoMargin;

	public set_document(document: DokumentoRender) {
		this.document = document;
	}
}

export { DokumentoPage };
