//Interface
import { IDokumentoReference } from '@/interface/reference.interface';
//Classes
import { DokumentoBase } from './base.core';
import { DokumentoRender } from '@/index';

class DokumentoReference extends DokumentoBase implements IDokumentoReference {
	constructor(data: IDokumentoReference) {
		super();
		this.document = data.document;
		this.id = data.id;
		this.gen = 0;
		this.data = data.data;
		if (!this.data.hasOwnProperty('length')) {
			this.data = { ...this.data, length: 0 };
		}
	}

	public document: DokumentoRender;
	public id: number;
	public gen: number;
	public data: any;

	public write(element: string | Buffer): Buffer {
		element = super.write(element);
		this.data.length += element.length;
		return element;
	}

	public toString() {
		return `${this.id} ${this.gen} R`;
	}
}

export { DokumentoReference };
