//Modules
//Interfaces
import { DokumentoFormat } from './interface/format.interface';
import { IDokumentoPage } from './interface/page.interface';
//Classess
import { DokumentoBase } from './core/base.core';
import { DokumentoPage } from './core/page.core';
//Utils
import { PDFVersions } from './util/versions.utils';

/**
 * DokumentoRender, objeto que renderiza un archivo pdf siguiendo el contenido
 * del formato Dokumento detallado a continuación:
 *
 * Características:
 * - Formato basado en Json.
 * - Imagenes contenidad como base64 o url de la ubicación.
 * - Texto mutlifuente y multiestilo.
 */
export class DokumentoRender extends DokumentoBase {
	/**
	 * Para crear renderizar un dokumento hay que primero cargar el contenido.
	 * @param data Un objeto Json del formato Dokumento
	 * @returns Un objeto DokumentoRender
	 */
	constructor(data: DokumentoFormat) {
		super();
		this.pdfVersion = data.pdfVersion ? (PDFVersions[data.pdfVersion] ? PDFVersions[data.pdfVersion] : 1.3) : 1.3;
		this.init_pages(data.pages ? data.pages : []);
		this.render();
	}

	public pdfVersion: number;
	public pages: DokumentoPage[] = [];
	public page: DokumentoPage = new DokumentoPage();
	public page_index: number = 0;

	public init_pages(pages: IDokumentoPage[]) {
		this.pages = pages.map(e => {
			const page = new DokumentoPage(e);
			page.set_document(this);
			return page;
		});
		if (this.pages) {
			this.page = this.pages[this.page_index];
		} else {
			this.page.set_document(this);
			this.pages = [this.page];
		}
	}

	public render() {
		this.render_header();
		this.render_elements();
		this.render_reference();
		this.render_trailer();
	}

	public render_header() {
		this.write(`%PDF-${this.pdfVersion}`);
		this.write('%\xFF\xFF\xFF\xFF');
	}

	public render_elements() {}

	public render_reference() {}

	public render_trailer() {

	}

	public toString() {
		return '[object DokumentoRender]';
	}
}
