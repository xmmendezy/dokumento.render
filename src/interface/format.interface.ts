//Interface
import { IDokumentoPage } from '@/interface/page.interface';

interface DokumentoFormat {
	pdfVersion?: string;
	pages?: IDokumentoPage[];
}

export { DokumentoFormat };
