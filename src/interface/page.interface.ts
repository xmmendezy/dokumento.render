//Interface
import { IDokumentoMargin } from '@/util/margin.util';

interface IDokumentoPage {
	size?: string;
	layout?: 'PORTRAIT' | 'LANDSPACE';
	margin?: IDokumentoMargin | number;
}

export { IDokumentoPage };
