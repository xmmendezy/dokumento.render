//Classes
import { DokumentoRender } from '@/index';

interface IDokumentoReference {
	document: DokumentoRender;
	id: number;
	data: any;
}

export { IDokumentoReference };
