interface IDokumentoMargin {
	top: number;
	bottom?: number;
	left: number;
	right?: number;
}

class DokumentoMargin implements IDokumentoMargin {
	constructor(data: IDokumentoMargin | number) {
		if (typeof data === 'number') {
			data = {
				top: data,
				left: data,
			};
		}
		this.top = data.top;
		this.bottom = data.bottom ? data.bottom : data.top;
		this.left = data.left;
		this.right = data.right ? data.right : data.left;
	}

	public top: number;
	public bottom: number;
	public left: number;
	public right: number;
}

const DEFAULT_MARGINS = new DokumentoMargin(80);

export { IDokumentoMargin, DokumentoMargin, DEFAULT_MARGINS };
