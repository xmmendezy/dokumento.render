//Classes
import { DokumentoReference } from '@/core/reference.core';

function pad(n: number, length: number) {
	return (Array(length + 1).join('0') + n).slice(-length);
}

const escapableRe = /[\n\r\t\b\f\(\)\\]/g;
const escapable = {
	'\n': '\\n',
	'\r': '\\r',
	'\t': '\\t',
	'\b': '\\b',
	'\f': '\\f',
	'\\': '\\\\',
	'(': '\\(',
	')': '\\)',
};

// Convert little endian UTF-16 to big endian
function swapBytes(buff: Buffer) {
	const l = buff.length;
	if (l & 0x01) {
		throw new Error('Buffer length must be even');
	} else {
		for (let i = 0, end = l - 1; i < end; i += 2) {
			const a = buff[i];
			buff[i] = buff[i + 1];
			buff[i + 1] = a;
		}
	}

	return buff;
}

function convertPDFObject(object: any) {
	if (typeof object === 'string') {
		return `/${object}`;
	} else if (object instanceof String) {
		let string = object;
		let isUnicode = false;
		for (let i = 0, end = string.length; i < end; i++) {
			if (string.charCodeAt(i) > 0x7f) {
				isUnicode = true;
				break;
			}
		}
		let stringBuffer;
		if (isUnicode) {
			stringBuffer = swapBytes(Buffer.from(`\ufeff${string}`, 'utf16le'));
		} else {
			stringBuffer = Buffer.from(string.valueOf(), 'ascii');
		}
		string = stringBuffer.toString('binary');
		string = string.replace(escapableRe, c => escapable[c]);
		return `(${string})`;
	} else if (Buffer.isBuffer(object)) {
		return `<${object.toString('hex')}>`;
	} else if (object instanceof DokumentoReference || object instanceof DokumentoReference) {
		return object.toString();
	} else if (object instanceof Date) {
		return `(D:${pad(object.getUTCFullYear(), 4)}/
		${pad(object.getUTCMonth() + 1, 2)}${pad(object.getUTCDate(), 2)}/
		${pad(object.getUTCHours(), 2)}${pad(object.getUTCMinutes(), 2)}/
		${pad(object.getUTCSeconds(), 2)}Z)`;
	} else if (Array.isArray(object)) {
		const items = object.map(e => convertPDFObject(e)).join(' ');
		return `[${items}]`;
	} else if ({}.toString.call(object) === '[object Object]') {
		const out = ['<<'];
		for (const key in object) {
			const val = object[key];
			out.push(`/${key} ${convertPDFObject(val)}`);
		}
		out.push('>>');
		return out.join('\n');
	} else if (typeof object === 'number') {
		return convertPDFnumber(object);
	} else {
		return `${object}`;
	}
}

function convertPDFnumber(n) {
	if (n > -1e21 && n < 1e21) {
		return Math.round(n * 1e6) / 1e6;
	}

	throw new Error(`unsupported number: ${n}`);
}

export { convertPDFObject, convertPDFnumber };
