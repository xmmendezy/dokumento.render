const PDFVersions: { [key: string]: number } = {
	'1.3': 1.3,
	'1.4': 1.4,
	'1.5': 1.5,
	'1.6': 1.6,
	'1.7': 1.7,
	'1.7ext3': 1.7,
};

export { PDFVersions };
