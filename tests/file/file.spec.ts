import { DokumentoRender } from '@/index';

describe('File 1 - simple archivo pdf', () => {
	it('File 1, render', () => {
		expect(new DokumentoRender({ pdfVersion: '1.7' })).toBeInstanceOf(DokumentoRender);
	});
});
